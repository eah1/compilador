%{
/*************************************************************************
Compiler for the Simple language
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/
/*=========================================================================
C Libraries, Symbol Table, Code Generator & other C code
=========================================================================*/
#include <stdio.h> /* For I/O */
#include <stdlib.h> /* For malloc here and in symbol table */
#include <string.h> /* For strcmp in symbol table */
#include "ST.h" /* Symbol Table */
#include "SM.h" /* Stack Machine */
#include "CG.h" /* Code Generator */

#define YYDEBUG 1 /* For Debugging */

int yyerror(char *);
int yylex();


int errors; /* Error Count */
int lenStr;
int inFunc = 0;
char *nameFunc;
int paramsCount = 0;
char paramsFunc[20][50];
/*-------------------------------------------------------------------------
The following support backpatching
-------------------------------------------------------------------------*/
struct lbs /* Labels for data, if and while */
{
  int for_goto;
  int for_jmp_false;
};

struct lbs * newlblrec() /* Allocate space for the labels */
{
   return (struct lbs *) malloc(sizeof(struct lbs));
}

/*-------------------------------------------------------------------------
Install identifier & check if previously defined.
-------------------------------------------------------------------------*/
void install ( char *sym_name, tipus state , int len)
{
  symrec *s = getsym (sym_name);
  if (s == 0)
    s = putsym (sym_name, state, len );
  else {
    if(len > 1){
      s = putsym (sym_name, state, len );
    }else{
      char message[ 100 ];
      sprintf( message, "%s is already defined\n", sym_name );
      yyerror( message );
    }
  }
}

/*-------------------------------------------------------------------------
If identifier is defined, generate code
-------------------------------------------------------------------------*/
int context_check( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->offset;
}

int context_length( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->len;
}

int context_type( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->state;
}

/*
  Quan modifiquen un string modificarem la longitud i el offset de la taula de symbol
*/
void modify_ST( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  identifier->offset = data_location(lenStr);
  if((identifier->state == eSTRING)){
    data_location_str(lenStr);
  }
  identifier->len = lenStr;
}

/*
  Guarda el espai a la pila i carrega el String
*/
void instString(char *id, char *ins){
  gen_code( DATA, strlen(ins) );
  install( id, eSTRING,strlen(ins));
  int i, value;
  char line[strlen(ins)];
  sprintf(line,"%s",ins);
  for(i=0;i<strlen(ins);i++){
    value = (int)line[i];
    gen_code( LD_INT, value );
    gen_code( STORE, context_check( id )+i );
  }
}

/*
  Per carregar el string a la pila
*/
void loadStr(char *id, int len){
  int i;
  for(i=0; i < context_length(id); i++){
    gen_code( LD_VAR, context_check( id )+i );
    gen_code( STORE, get_data_offset() + i + len );

  }
}



/*=========================================================================
SEMANTIC RECORDS
=========================================================================*/
%}

%union /* The Semantic Records */
 {
   int intval; /* Integer values */
   char *id; /* Identifiers */
   struct lbs *lbls; /* For backpatching */
   char *cadena; /* String */
};

/*=========================================================================
TOKENS
=========================================================================*/
%start program
%token <intval> NUMBER LENGTH /* Simple integer */
%token <id> IDENTIFIER PARAM /* Simple identifier */
%token <cadena> INSIDE DATAFUNC/* Simple identifier */
%token <lbls> IF WHILE TYPE_RETURN /* For backpatching labels */
%token SKIP THEN ELSE FI DO END RETURN
%token INTEGER READ WRITE LET IN STR VEC CONCAT EQUALS
%token ASSGNOP
%token INIFUNC FIFUNC

/*=========================================================================
OPERATOR PRECEDENCE
=========================================================================*/
%left '-' '+'
%left '*' '/'
%right '^'

/*=========================================================================
GRAMMAR RULES for the Simple language
=========================================================================*/

%%

/*
* Regles per a les Funcions
*/

program : funcdeclarations LET declarations IN {  }
          commands END { gen_code( HALT, 0 ); YYACCEPT; }
;

funcdeclarations:
    | funcdeclaration funcdeclaration;
funcdeclaration:
    | TYPE_RETURN { inFunc=1; } IDENTIFIER {
          install($3, eFUNC, 0);
          $1 = (struct lbs *) newlblrec();
          $1->for_goto = reserve_loc();
        }params INIFUNC declarations commands FIFUNC {
                  gen_code(RET, 0);
                  back_patch( $1->for_goto, GOTO, gen_label() );
                  inFunc = 0;
                  symrec *s = getsym ($3);
                  putsymFunc (s, paramsFunc);
                  int i;
                  for(i = 0; i < 20; i++){
                    strcpy(paramsFunc[i], "");
                  }
                  paramsCount=0;
        };

params:
    | '(' paramfunction ')' {  }

paramfunction :
    | paramfunction ':' INTEGER IDENTIFIER  {
                char name[strlen($4)];
                sprintf(name,"f%s",$4);
                install(name, eINT,1);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
    | INTEGER IDENTIFIER {
                char name[strlen($2)];
                sprintf(name,"f%s",$2);
                install(name, eINT,1);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
    | paramfunction ':' STR IDENTIFIER '[' NUMBER ']' {
                char name[strlen($4)];
                sprintf(name,"f%s",$4);
                install(name, eVEC,$6);
                symrec *s = getsym (name);
                s->state = eSTRING;
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
    | STR IDENTIFIER '[' NUMBER ']' {
                char name[strlen($2)];
                sprintf(name,"f%s",$2);
                install(name, eVEC,$4);
                symrec *s = getsym (name);
                s->state = eSTRING;
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
    | paramfunction ':' VEC IDENTIFIER '[' NUMBER ']' {
                char name[strlen($4)];
                sprintf(name,"f%s",$4);
                install(name, eVEC,$6);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
    | VEC IDENTIFIER '[' NUMBER ']' {
                char name[strlen($2)];
                sprintf(name,"f%s",$2);
                install(name, eVEC,$4);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
;
/*
* Regles per a les declaracions
*/

declarations : /* empty */
    | declarations INTEGER id_seq_int IDENTIFIER '.' {
          install( $4, eINT,1);
          gen_code( DATA, data_location(0)-get_data_offset_str() );
        }
    | declarations STR id_seq_string IDENTIFIER ASSGNOP INSIDE '.' {
            instString($4,$6);
          }
    | declarations VEC id_seq_vector IDENTIFIER '[' NUMBER ']' '.' {
            gen_code( DATA, $6 );
            install( $4, eVEC, $6);
          }
;

id_seq_int : /* empty */
    | id_seq_int IDENTIFIER ',' { install( $2, eINT,1 ); }
;

id_seq_string : /* empty */
    | id_seq_string IDENTIFIER ASSGNOP INSIDE ',' {
      instString($2,$4);
    }
;

id_seq_vector : /* empty */
    | id_seq_vector IDENTIFIER '[' NUMBER ']' ',' {
            gen_code( DATA, $4 );
            install( $2, eVEC,$4 );
          }
;

/*
* Regles per comandes i expressions
*/

commands : /* empty */
    | commands command ';'
;

command : SKIP
   | READ IDENTIFIER {
            char name[strlen($2)];
            sprintf(name,"%s",$2);
          if(inFunc==1){
            sprintf(name,"f%s",$2);
          }
          gen_code( READ_INT, context_check( name ) );
      }
   | WRITE exp { gen_code( WRITE_INT, 0 ); }
   | WRITE '(' IDENTIFIER ')' {
            char name[strlen($3)];
            sprintf(name,"%s",$3);
            if(inFunc==1){
              sprintf(name,"%c%s",'f',$3);
            }
            int i, l=0;
            if(context_type(name)==eVEC){
              gen_code( VEC_INI, 0 );
              l=1;
            }
            for(i=0;i<context_length(name)-l; i++){
              gen_code( LD_VAR, context_check( name ) + i );
              if(context_type(name)==eSTRING){
                gen_code( WRITE_STR, 0 );
              }else{
                gen_code( WRITE_VEC, 0 );
              }
            }
            if(context_type(name)==eVEC){
              gen_code( LD_VAR, context_check( name ) + i );
              gen_code( VEC_END, 0 );
            }
            gen_code(END_LINE,0);
        }
   | IDENTIFIER ASSGNOP exp {
            char name[strlen($1)];
            sprintf(name,"%s",$1);
          if(inFunc==1){
            sprintf(name,"f%s",$1);
          }
          if(context_type(name) == eSTRING || context_type(name) == eVEC){
            modify_ST(name);
            lenStr=0;
          }else{
            gen_code( STORE, context_check( name ) );
          }
        }
    | IDENTIFIER '@' IDENTIFIER { nameFunc=$3; } valuesFunc FIFUNC {
         printf("ASD\n");
         /*paramsCount=0;
         gen_code( CALL, context_check(nameFunc)+1);
         if(context_type($1) == eINT){
             gen_code( STORE, context_check( $1 ) );
         } else {
             int i;
             for(i = context_length($1)-1; i >= 0; i--){
               gen_code( STORE, context_check($1) + i);
             }
         }*/
    }
   | IDENTIFIER '[' NUMBER ']' ASSGNOP exp {
            char name[strlen($1)];
            sprintf(name,"%s",$1);
          if(inFunc==1){
            sprintf(name,"f%s",$1);
          }
           if(context_length(name)<$3 || $3>=0){
             gen_code( STORE, context_check( name ) + $3 );
           }
        }
   | IDENTIFIER { nameFunc=$1; } valuesFunc FIFUNC {
        paramsCount=0;
        gen_code( CALL, context_check(nameFunc)+1);
   }
   | RETURN IDENTIFIER {
     char name[strlen($2)];
     sprintf(name,"%s",$2);
     if(inFunc==1){
        sprintf(name,"f%s",$2);
     }
     if(context_type(name) == eINT){
         gen_code( LD_VAR, context_check( name ) );
     } else {
         int i;
         for(i = 0; i < context_length(name); i++){
           gen_code( LD_VAR, context_check(name) + i);
         }
     }
   }
   | IF bool_exp { $1 = (struct lbs *) newlblrec(); $1->for_jmp_false = reserve_loc(); }
   THEN commands { $1->for_goto = reserve_loc(); } ELSE {
     back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() );
   } commands FI { back_patch( $1->for_goto, GOTO, gen_label() ); }
   | WHILE { $1 = (struct lbs *) newlblrec(); $1->for_goto = gen_label(); }
   bool_exp { $1->for_jmp_false = reserve_loc(); } DO commands END { gen_code( GOTO, $1->for_goto );
   back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() ); }
;

valuesFunc:
  | INIFUNC IDENTIFIER {
      symrec *s = getsym(nameFunc);
      s->len = context_length($2);
      if(context_type($2) == eINT){
          gen_code( LD_VAR, context_check( $2 ) );
          gen_code( STORE, context_check( s->params[paramsCount] ) );
      } else {
          int i;
          for(i = 0; i < context_length($2); i++){
            gen_code( LD_VAR, context_check($2) + i);
            gen_code( STORE, context_check( s->params[paramsCount]) + i);
          }
      }
      paramsCount+=1;
      }
  | INIFUNC { }
  | valuesFunc ':' IDENTIFIER {
    symrec *s = getsym(nameFunc);
    s->len = context_length($3);
    if(context_type($3) == eINT){
        gen_code( LD_VAR, context_check( $3 ) );
        gen_code( STORE, context_check( s->params[paramsCount] ) );
    } else {
        int i;
        for(i = 0; i < context_length($3); i++){
          gen_code( LD_VAR, context_check($3) + i);
          gen_code( STORE, context_check( s->params[paramsCount]) + i);
        }
    }
    paramsCount+=1;
  }

/* TODO  equals per vectors i strings */

bool_exp : exp '<' exp { gen_code( LT, 0 ); }
   | exp '=' exp { gen_code( EQ, 0 ); }
   | exp '>' exp { gen_code( GT, 0 ); }
;

exp : NUMBER { gen_code( LD_INT, $1 ); }
   | LENGTH '(' IDENTIFIER ')' { gen_code( LD_INT, context_length($3) ); }
   | IDENTIFIER {
          char name[strlen($1)];
          sprintf(name,"%s",$1);
          if(inFunc==1){
            sprintf(name,"f%s",$1);
          }
          if(context_type(name) == eINT){
            gen_code( LD_VAR, context_check( name ) );
          }
        }
   | IDENTIFIER '[' NUMBER ']' {
         char name[strlen($1)];
         sprintf(name,"%s",$1);
         if(inFunc==1){
           sprintf(name,"f%s",$1);
         }
          gen_code( LD_VAR, context_check(name)+$3 ); }
   | exp '+' exp { gen_code( ADD, 0 ); }
   | exp '-' exp { gen_code( SUB, 0 ); }
   | exp '*' exp { gen_code( MULT, 0 ); }
   | exp '/' exp { gen_code( DIV, 0 ); }
   | exp '^' exp { gen_code( PWR, 0 ); }
   | CONCAT '(' IDENTIFIER ',' IDENTIFIER ')' {
            char name[strlen($3)];
            sprintf(name,"%s",$3);
            char name2[strlen($5)];
            sprintf(name2,"%s",$5);
            if(inFunc==1){
             sprintf(name,"f%s",$3);
             sprintf(name2,"f%s",$5);
            }
            if(context_type(name)!= eINT && context_type(name2)!= eINT && context_type(name) == context_type(name2)){
              lenStr = context_length(name)+context_length(name2);
              gen_code( DATA, lenStr );
              loadStr(name,0);
              loadStr(name2,context_length(name));
            }
       }
   | '(' exp ')'
;

%%

extern struct instruction code[ MAX_MEMORY ];

/*=========================================================================
MAIN
=========================================================================*/
int main( int argc, char *argv[] )
{
  extern FILE *yyin;
  if ( argc < 3 ) {
    printf("usage <input-file> <output-file>\n");
    return -1;
  }
  yyin = fopen( argv[1], "r" );
  /*yydebug = 1;*/
  errors = 0;
  printf("Senzill Compiler\n");
  yyparse ();
  printf ( "Parse Completed\n" );
  if ( errors == 0 )
    {
      //print_code ();
      //fetch_execute_cycle();
      write_bytecode( argv[2] );
    }
  return 0;
}

/*=========================================================================
YYERROR
=========================================================================*/
int yyerror ( char *s ) /* Called by yyparse on error */
{
  errors++;
  printf ("%s\n", s);
  return 0;
}
/**************************** End Grammar File ***************************/
