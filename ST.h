/*-------------------------------------------------------------------------
SYMBOL TABLE RECORD
-------------------------------------------------------------------------*/

typedef enum { eINT, eSTRING, eVEC, eFUNC } tipus;

struct symrec
{
  char *name; /* name of symbol */
  int offset; /* data offset */
  int len; /* len variable o return function */
  tipus state;
  char params[20][50];
  struct symrec *next; /* link field */
};
typedef struct symrec symrec;

symrec * getsym (char *sym_name);
symrec * putsym (char *sym_name, tipus state, int len);
symrec * putsymFunc (symrec *ptr, char params[20][50]);
