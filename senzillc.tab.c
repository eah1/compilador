/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "senzillc.y" /* yacc.c:339  */

/*************************************************************************
Compiler for the Simple language
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/
/*=========================================================================
C Libraries, Symbol Table, Code Generator & other C code
=========================================================================*/
#include <stdio.h> /* For I/O */
#include <stdlib.h> /* For malloc here and in symbol table */
#include <string.h> /* For strcmp in symbol table */
#include "ST.h" /* Symbol Table */
#include "SM.h" /* Stack Machine */
#include "CG.h" /* Code Generator */

#define YYDEBUG 1 /* For Debugging */

int yyerror(char *);
int yylex();


int errors; /* Error Count */
int lenStr;
int inFunc = 0;
char *nameFunc;
int paramsCount = 0;
char paramsFunc[20][50];
/*-------------------------------------------------------------------------
The following support backpatching
-------------------------------------------------------------------------*/
struct lbs /* Labels for data, if and while */
{
  int for_goto;
  int for_jmp_false;
};

struct lbs * newlblrec() /* Allocate space for the labels */
{
   return (struct lbs *) malloc(sizeof(struct lbs));
}

/*-------------------------------------------------------------------------
Install identifier & check if previously defined.
-------------------------------------------------------------------------*/
void install ( char *sym_name, tipus state , int len)
{
  symrec *s = getsym (sym_name);
  if (s == 0)
    s = putsym (sym_name, state, len );
  else {
    if(len > 1){
      s = putsym (sym_name, state, len );
    }else{
      char message[ 100 ];
      sprintf( message, "%s is already defined\n", sym_name );
      yyerror( message );
    }
  }
}

/*-------------------------------------------------------------------------
If identifier is defined, generate code
-------------------------------------------------------------------------*/
int context_check( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->offset;
}

int context_length( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->len;
}

int context_type( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  return identifier->state;
}

/*
  Quan modifiquen un string modificarem la longitud i el offset de la taula de symbol
*/
void modify_ST( char *sym_name ){
  symrec *identifier = getsym( sym_name );
  identifier->offset = data_location(lenStr);
  if((identifier->state == eSTRING)){
    data_location_str(lenStr);
  }
  identifier->len = lenStr;
}

/*
  Guarda el espai a la pila i carrega el String
*/
void instString(char *id, char *ins){
  gen_code( DATA, strlen(ins) );
  install( id, eSTRING,strlen(ins));
  int i, value;
  char line[strlen(ins)];
  sprintf(line,"%s",ins);
  for(i=0;i<strlen(ins);i++){
    value = (int)line[i];
    gen_code( LD_INT, value );
    gen_code( STORE, context_check( id )+i );
  }
}

/*
  Per carregar el string a la pila
*/
void loadStr(char *id, int len){
  int i;
  for(i=0; i < context_length(id); i++){
    gen_code( LD_VAR, context_check( id )+i );
    gen_code( STORE, get_data_offset() + i + len );

  }
}



/*=========================================================================
SEMANTIC RECORDS
=========================================================================*/

#line 192 "senzillc.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "senzillc.tab.h".  */
#ifndef YY_YY_SENZILLC_TAB_H_INCLUDED
# define YY_YY_SENZILLC_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUMBER = 258,
    LENGTH = 259,
    IDENTIFIER = 260,
    PARAM = 261,
    INSIDE = 262,
    DATAFUNC = 263,
    IF = 264,
    WHILE = 265,
    TYPE_RETURN = 266,
    SKIP = 267,
    THEN = 268,
    ELSE = 269,
    FI = 270,
    DO = 271,
    END = 272,
    RETURN = 273,
    INTEGER = 274,
    READ = 275,
    WRITE = 276,
    LET = 277,
    IN = 278,
    STR = 279,
    VEC = 280,
    CONCAT = 281,
    EQUALS = 282,
    ASSGNOP = 283,
    INIFUNC = 284,
    FIFUNC = 285
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 128 "senzillc.y" /* yacc.c:355  */

   int intval; /* Integer values */
   char *id; /* Identifiers */
   struct lbs *lbls; /* For backpatching */
   char *cadena; /* String */

#line 270 "senzillc.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SENZILLC_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 285 "senzillc.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   193

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  48
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  25
/* YYNRULES -- Number of rules.  */
#define YYNRULES  66
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  153

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   285

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      36,    37,    33,    32,    42,    31,    41,    34,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    38,    43,
      45,    46,    47,     2,    44,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    39,     2,    40,    35,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   165,   165,   165,   169,   170,   171,   172,   172,   172,
     189,   190,   192,   193,   200,   207,   216,   225,   232,   244,
     245,   249,   252,   258,   259,   262,   263,   268,   269,   279,
     280,   283,   284,   292,   293,   318,   331,   331,   344,   354,
     354,   358,   373,   374,   374,   373,   377,   378,   377,   382,
     383,   398,   399,   417,   418,   419,   422,   423,   424,   434,
     441,   442,   443,   444,   445,   446,   462
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMBER", "LENGTH", "IDENTIFIER",
  "PARAM", "INSIDE", "DATAFUNC", "IF", "WHILE", "TYPE_RETURN", "SKIP",
  "THEN", "ELSE", "FI", "DO", "END", "RETURN", "INTEGER", "READ", "WRITE",
  "LET", "IN", "STR", "VEC", "CONCAT", "EQUALS", "ASSGNOP", "INIFUNC",
  "FIFUNC", "'-'", "'+'", "'*'", "'/'", "'^'", "'('", "')'", "':'", "'['",
  "']'", "'.'", "','", "';'", "'@'", "'<'", "'='", "'>'", "$accept",
  "program", "$@1", "funcdeclarations", "funcdeclaration", "$@2", "$@3",
  "params", "paramfunction", "declarations", "id_seq_int", "id_seq_string",
  "id_seq_vector", "commands", "command", "$@4", "$@5", "$@6", "$@7",
  "$@8", "$@9", "$@10", "valuesFunc", "bool_exp", "exp", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,    45,    43,    42,    47,    94,    40,    41,    58,    91,
      93,    46,    44,    59,    64,    60,    61,    62
};
# endif

#define YYPACT_NINF -46

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-46)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -2,   -46,    20,     6,    -2,    26,   -46,   -46,   -46,   -46,
     109,     1,   -46,   -46,   -46,   -46,    38,     5,    35,   -46,
      36,    39,    55,    63,    68,    18,   -46,    23,    49,    52,
      46,   -46,    54,    69,   -46,    81,   111,   -46,   -46,    -1,
       0,   -46,   -46,   -46,    77,    91,     3,    66,   122,   114,
     128,   134,   141,   142,   143,    12,     0,   146,   145,   123,
     -46,   115,   116,   117,     0,   -46,    43,     0,   -46,   -46,
       9,   107,   -46,    30,   118,   119,   120,   -46,   124,   125,
     -46,   107,   121,   -46,   149,   -28,   151,   154,   157,    88,
     152,     0,     0,     0,     0,     0,     0,     0,     0,   -46,
     -21,   -46,   -46,    57,   -46,   -46,   163,   164,   140,   123,
     -46,   -46,   165,   132,   131,   130,   -46,   -46,   110,   110,
     138,   138,   138,   107,   107,   107,   158,   -46,   -46,   -46,
     135,   136,     0,   -15,   -46,   -46,   -46,   172,   106,   -46,
     -46,   -46,   107,   -46,   144,   166,    74,   -46,   -46,   -46,
     -46,    92,   -46
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     7,     0,     0,     6,     0,     1,    19,     5,     8,
       0,    10,    23,     2,    25,    27,    12,     0,     0,    29,
       0,     0,     0,     0,     0,     0,    19,     0,     0,     0,
       0,    14,     0,     0,    11,     0,    29,    20,    24,    39,
       0,    46,    31,     3,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    49,
      56,     0,    58,     0,     0,    42,     0,     0,    41,    32,
       0,    33,    30,     0,     0,     0,     0,    13,     0,     0,
       9,    35,     0,    36,    51,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    47,
      58,    21,    26,     0,    16,    18,     0,     0,     0,    49,
      50,    40,     0,     0,     0,     0,    66,    29,    61,    60,
      62,    63,    64,    53,    54,    55,     0,    34,    22,    28,
       0,     0,     0,     0,    52,    57,    59,     0,    43,    29,
      15,    17,    38,    37,     0,     0,     0,    65,    44,    48,
      29,     0,    45
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -46,   -46,   -46,   -46,   174,   -46,   -46,   -46,   -46,   153,
     -46,   -46,   -46,   -36,   -46,   -46,   -46,   -46,   -46,   -46,
     -46,   -46,    73,   126,   -45
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,    19,     3,     4,     5,    11,    17,    25,    10,
      18,    20,    21,    28,    47,   109,    59,    90,   145,   150,
      67,   126,    85,    65,    66
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      55,    71,   111,    60,    61,    62,    60,    61,    62,     1,
     112,    81,    60,    61,   100,   143,   127,    39,    87,    89,
       6,    40,    41,   112,    42,    89,    63,    56,     7,    63,
      44,     9,    45,    46,    26,    63,    64,    16,    57,    70,
      27,    29,    80,    58,    30,    64,   118,   119,   120,   121,
     122,   123,   124,   125,    39,    34,    35,    22,    40,    41,
      31,    42,    23,    24,    37,    38,    43,    44,    32,    45,
      46,   101,   102,    33,    91,    92,    93,    94,    95,    39,
      48,   138,    68,    40,    41,    49,    42,   142,    96,    97,
      98,   149,    44,    50,    45,    46,    69,    39,   128,   129,
      52,    40,    41,   146,    42,    53,    54,   152,    51,    72,
      44,    39,    45,    46,   151,    40,    41,    74,    42,    91,
      92,    93,    94,    95,    44,   116,    45,    46,    12,    73,
      12,    75,    13,    14,    15,    14,    15,    76,    91,    92,
      93,    94,    95,    93,    94,    95,    77,    78,    79,    82,
      83,    86,    84,    88,   110,    87,   113,   114,   103,   104,
     105,   108,   115,   106,   107,   117,   130,   131,   132,   135,
     134,   136,   137,    95,   139,   140,   141,   144,     8,    36,
     148,   147,   133,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    99
};

static const yytype_int16 yycheck[] =
{
      36,    46,    30,     3,     4,     5,     3,     4,     5,    11,
      38,    56,     3,     4,     5,    30,    37,     5,    39,    64,
       0,     9,    10,    38,    12,    70,    26,    28,    22,    26,
      18,     5,    20,    21,    29,    26,    36,    36,    39,    36,
       5,     5,    30,    44,     5,    36,    91,    92,    93,    94,
      95,    96,    97,    98,     5,    37,    38,    19,     9,    10,
       5,    12,    24,    25,    41,    42,    17,    18,     5,    20,
      21,    41,    42,     5,    31,    32,    33,    34,    35,     5,
      28,   117,     5,     9,    10,    39,    12,   132,    45,    46,
      47,    17,    18,    39,    20,    21,     5,     5,    41,    42,
      19,     9,    10,   139,    12,    24,    25,    15,    39,    43,
      18,     5,    20,    21,   150,     9,    10,     3,    12,    31,
      32,    33,    34,    35,    18,    37,    20,    21,    19,     7,
      19,     3,    23,    24,    25,    24,    25,     3,    31,    32,
      33,    34,    35,    33,    34,    35,     5,     5,     5,     3,
       5,    36,    29,    36,     5,    39,     5,     3,    40,    40,
      40,    40,     5,    39,    39,    13,     3,     3,    28,    37,
       5,    40,    42,    35,    16,    40,    40,     5,     4,    26,
      14,    37,   109,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    67
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    11,    49,    51,    52,    53,     0,    22,    52,     5,
      57,    54,    19,    23,    24,    25,    36,    55,    58,    50,
      59,    60,    19,    24,    25,    56,    29,     5,    61,     5,
       5,     5,     5,     5,    37,    38,    57,    41,    42,     5,
       9,    10,    12,    17,    18,    20,    21,    62,    28,    39,
      39,    39,    19,    24,    25,    61,    28,    39,    44,    64,
       3,     4,     5,    26,    36,    71,    72,    68,     5,     5,
      36,    72,    43,     7,     3,     3,     3,     5,     5,     5,
      30,    72,     3,     5,    29,    70,    36,    39,    36,    72,
      65,    31,    32,    33,    34,    35,    45,    46,    47,    71,
       5,    41,    42,    40,    40,    40,    39,    39,    40,    63,
       5,    30,    38,     5,     3,     5,    37,    13,    72,    72,
      72,    72,    72,    72,    72,    72,    69,    37,    41,    42,
       3,     3,    28,    70,     5,    37,    40,    42,    61,    16,
      40,    40,    72,    30,     5,    66,    61,    37,    14,    17,
      67,    61,    15
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    48,    50,    49,    51,    51,    52,    53,    54,    52,
      55,    55,    56,    56,    56,    56,    56,    56,    56,    57,
      57,    57,    57,    58,    58,    59,    59,    60,    60,    61,
      61,    62,    62,    62,    62,    62,    63,    62,    62,    64,
      62,    62,    65,    66,    67,    62,    68,    69,    62,    70,
      70,    70,    70,    71,    71,    71,    72,    72,    72,    72,
      72,    72,    72,    72,    72,    72,    72
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     7,     0,     2,     0,     0,     0,     9,
       0,     3,     0,     4,     2,     7,     5,     7,     5,     0,
       5,     7,     8,     0,     3,     0,     5,     0,     6,     0,
       3,     1,     2,     2,     4,     3,     0,     6,     6,     0,
       4,     2,     0,     0,     0,    10,     0,     0,     7,     0,
       2,     1,     3,     3,     3,     3,     1,     4,     1,     4,
       3,     3,     3,     3,     3,     6,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 165 "senzillc.y" /* yacc.c:1646  */
    {  }
#line 1470 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 166 "senzillc.y" /* yacc.c:1646  */
    { gen_code( HALT, 0 ); YYACCEPT; }
#line 1476 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 172 "senzillc.y" /* yacc.c:1646  */
    { inFunc=1; }
#line 1482 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 172 "senzillc.y" /* yacc.c:1646  */
    {
          install((yyvsp[0].id), eFUNC, 0);
          (yyvsp[-2].lbls) = (struct lbs *) newlblrec();
          (yyvsp[-2].lbls)->for_goto = reserve_loc();
        }
#line 1492 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 176 "senzillc.y" /* yacc.c:1646  */
    {
                  gen_code(RET, 0);
                  back_patch( (yyvsp[-8].lbls)->for_goto, GOTO, gen_label() );
                  inFunc = 0;
                  symrec *s = getsym ((yyvsp[-6].id));
                  putsymFunc (s, paramsFunc);
                  int i;
                  for(i = 0; i < 20; i++){
                    strcpy(paramsFunc[i], "");
                  }
                  paramsCount=0;
        }
#line 1509 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 190 "senzillc.y" /* yacc.c:1646  */
    {  }
#line 1515 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 193 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[0].id))];
                sprintf(name,"f%s",(yyvsp[0].id));
                install(name, eINT,1);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1527 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 200 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[0].id))];
                sprintf(name,"f%s",(yyvsp[0].id));
                install(name, eINT,1);
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1539 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 207 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[-3].id))];
                sprintf(name,"f%s",(yyvsp[-3].id));
                install(name, eVEC,(yyvsp[-1].intval));
                symrec *s = getsym (name);
                s->state = eSTRING;
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1553 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 216 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[-3].id))];
                sprintf(name,"f%s",(yyvsp[-3].id));
                install(name, eVEC,(yyvsp[-1].intval));
                symrec *s = getsym (name);
                s->state = eSTRING;
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1567 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 225 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[-3].id))];
                sprintf(name,"f%s",(yyvsp[-3].id));
                install(name, eVEC,(yyvsp[-1].intval));
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1579 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 232 "senzillc.y" /* yacc.c:1646  */
    {
                char name[strlen((yyvsp[-3].id))];
                sprintf(name,"f%s",(yyvsp[-3].id));
                install(name, eVEC,(yyvsp[-1].intval));
                strcpy(paramsFunc[paramsCount], name);
                paramsCount+=1;
              }
#line 1591 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 245 "senzillc.y" /* yacc.c:1646  */
    {
          install( (yyvsp[-1].id), eINT,1);
          gen_code( DATA, data_location(0)-get_data_offset_str() );
        }
#line 1600 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 249 "senzillc.y" /* yacc.c:1646  */
    {
            instString((yyvsp[-3].id),(yyvsp[-1].cadena));
          }
#line 1608 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 252 "senzillc.y" /* yacc.c:1646  */
    {
            gen_code( DATA, (yyvsp[-2].intval) );
            install( (yyvsp[-4].id), eVEC, (yyvsp[-2].intval));
          }
#line 1617 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 259 "senzillc.y" /* yacc.c:1646  */
    { install( (yyvsp[-1].id), eINT,1 ); }
#line 1623 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 263 "senzillc.y" /* yacc.c:1646  */
    {
      instString((yyvsp[-3].id),(yyvsp[-1].cadena));
    }
#line 1631 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 269 "senzillc.y" /* yacc.c:1646  */
    {
            gen_code( DATA, (yyvsp[-2].intval) );
            install( (yyvsp[-4].id), eVEC,(yyvsp[-2].intval) );
          }
#line 1640 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 284 "senzillc.y" /* yacc.c:1646  */
    {
            char name[strlen((yyvsp[0].id))];
            sprintf(name,"%s",(yyvsp[0].id));
          if(inFunc==1){
            sprintf(name,"f%s",(yyvsp[0].id));
          }
          gen_code( READ_INT, context_check( name ) );
      }
#line 1653 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 292 "senzillc.y" /* yacc.c:1646  */
    { gen_code( WRITE_INT, 0 ); }
#line 1659 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 293 "senzillc.y" /* yacc.c:1646  */
    {
            char name[strlen((yyvsp[-1].id))];
            sprintf(name,"%s",(yyvsp[-1].id));
            if(inFunc==1){
              sprintf(name,"%c%s",'f',(yyvsp[-1].id));
            }
            int i, l=0;
            if(context_type(name)==eVEC){
              gen_code( VEC_INI, 0 );
              l=1;
            }
            for(i=0;i<context_length(name)-l; i++){
              gen_code( LD_VAR, context_check( name ) + i );
              if(context_type(name)==eSTRING){
                gen_code( WRITE_STR, 0 );
              }else{
                gen_code( WRITE_VEC, 0 );
              }
            }
            if(context_type(name)==eVEC){
              gen_code( LD_VAR, context_check( name ) + i );
              gen_code( VEC_END, 0 );
            }
            gen_code(END_LINE,0);
        }
#line 1689 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 318 "senzillc.y" /* yacc.c:1646  */
    {
            char name[strlen((yyvsp[-2].id))];
            sprintf(name,"%s",(yyvsp[-2].id));
          if(inFunc==1){
            sprintf(name,"f%s",(yyvsp[-2].id));
          }
          if(context_type(name) == eSTRING || context_type(name) == eVEC){
            modify_ST(name);
            lenStr=0;
          }else{
            gen_code( STORE, context_check( name ) );
          }
        }
#line 1707 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 331 "senzillc.y" /* yacc.c:1646  */
    { nameFunc=(yyvsp[0].id); }
#line 1713 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 331 "senzillc.y" /* yacc.c:1646  */
    {
         printf("ASD\n");
         /*paramsCount=0;
         gen_code( CALL, context_check(nameFunc)+1);
         if(context_type($1) == eINT){
             gen_code( STORE, context_check( $1 ) );
         } else {
             int i;
             for(i = context_length($1)-1; i >= 0; i--){
               gen_code( STORE, context_check($1) + i);
             }
         }*/
    }
#line 1731 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 344 "senzillc.y" /* yacc.c:1646  */
    {
            char name[strlen((yyvsp[-5].id))];
            sprintf(name,"%s",(yyvsp[-5].id));
          if(inFunc==1){
            sprintf(name,"f%s",(yyvsp[-5].id));
          }
           if(context_length(name)<(yyvsp[-3].intval) || (yyvsp[-3].intval)>=0){
             gen_code( STORE, context_check( name ) + (yyvsp[-3].intval) );
           }
        }
#line 1746 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 354 "senzillc.y" /* yacc.c:1646  */
    { nameFunc=(yyvsp[0].id); }
#line 1752 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 354 "senzillc.y" /* yacc.c:1646  */
    {
        paramsCount=0;
        gen_code( CALL, context_check(nameFunc)+1);
   }
#line 1761 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 358 "senzillc.y" /* yacc.c:1646  */
    {
     char name[strlen((yyvsp[0].id))];
     sprintf(name,"%s",(yyvsp[0].id));
     if(inFunc==1){
        sprintf(name,"f%s",(yyvsp[0].id));
     }
     if(context_type(name) == eINT){
         gen_code( LD_VAR, context_check( name ) );
     } else {
         int i;
         for(i = 0; i < context_length(name); i++){
           gen_code( LD_VAR, context_check(name) + i);
         }
     }
   }
#line 1781 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 373 "senzillc.y" /* yacc.c:1646  */
    { (yyvsp[-1].lbls) = (struct lbs *) newlblrec(); (yyvsp[-1].lbls)->for_jmp_false = reserve_loc(); }
#line 1787 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 374 "senzillc.y" /* yacc.c:1646  */
    { (yyvsp[-4].lbls)->for_goto = reserve_loc(); }
#line 1793 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 374 "senzillc.y" /* yacc.c:1646  */
    {
     back_patch( (yyvsp[-6].lbls)->for_jmp_false, JMP_FALSE, gen_label() );
   }
#line 1801 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 376 "senzillc.y" /* yacc.c:1646  */
    { back_patch( (yyvsp[-9].lbls)->for_goto, GOTO, gen_label() ); }
#line 1807 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 377 "senzillc.y" /* yacc.c:1646  */
    { (yyvsp[0].lbls) = (struct lbs *) newlblrec(); (yyvsp[0].lbls)->for_goto = gen_label(); }
#line 1813 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 378 "senzillc.y" /* yacc.c:1646  */
    { (yyvsp[-2].lbls)->for_jmp_false = reserve_loc(); }
#line 1819 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 378 "senzillc.y" /* yacc.c:1646  */
    { gen_code( GOTO, (yyvsp[-6].lbls)->for_goto );
   back_patch( (yyvsp[-6].lbls)->for_jmp_false, JMP_FALSE, gen_label() ); }
#line 1826 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 383 "senzillc.y" /* yacc.c:1646  */
    {
      symrec *s = getsym(nameFunc);
      s->len = context_length((yyvsp[0].id));
      if(context_type((yyvsp[0].id)) == eINT){
          gen_code( LD_VAR, context_check( (yyvsp[0].id) ) );
          gen_code( STORE, context_check( s->params[paramsCount] ) );
      } else {
          int i;
          for(i = 0; i < context_length((yyvsp[0].id)); i++){
            gen_code( LD_VAR, context_check((yyvsp[0].id)) + i);
            gen_code( STORE, context_check( s->params[paramsCount]) + i);
          }
      }
      paramsCount+=1;
      }
#line 1846 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 398 "senzillc.y" /* yacc.c:1646  */
    { }
#line 1852 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 399 "senzillc.y" /* yacc.c:1646  */
    {
    symrec *s = getsym(nameFunc);
    s->len = context_length((yyvsp[0].id));
    if(context_type((yyvsp[0].id)) == eINT){
        gen_code( LD_VAR, context_check( (yyvsp[0].id) ) );
        gen_code( STORE, context_check( s->params[paramsCount] ) );
    } else {
        int i;
        for(i = 0; i < context_length((yyvsp[0].id)); i++){
          gen_code( LD_VAR, context_check((yyvsp[0].id)) + i);
          gen_code( STORE, context_check( s->params[paramsCount]) + i);
        }
    }
    paramsCount+=1;
  }
#line 1872 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 417 "senzillc.y" /* yacc.c:1646  */
    { gen_code( LT, 0 ); }
#line 1878 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 418 "senzillc.y" /* yacc.c:1646  */
    { gen_code( EQ, 0 ); }
#line 1884 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 419 "senzillc.y" /* yacc.c:1646  */
    { gen_code( GT, 0 ); }
#line 1890 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 422 "senzillc.y" /* yacc.c:1646  */
    { gen_code( LD_INT, (yyvsp[0].intval) ); }
#line 1896 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 423 "senzillc.y" /* yacc.c:1646  */
    { gen_code( LD_INT, context_length((yyvsp[-1].id)) ); }
#line 1902 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 424 "senzillc.y" /* yacc.c:1646  */
    {
          char name[strlen((yyvsp[0].id))];
          sprintf(name,"%s",(yyvsp[0].id));
          if(inFunc==1){
            sprintf(name,"f%s",(yyvsp[0].id));
          }
          if(context_type(name) == eINT){
            gen_code( LD_VAR, context_check( name ) );
          }
        }
#line 1917 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 434 "senzillc.y" /* yacc.c:1646  */
    {
         char name[strlen((yyvsp[-3].id))];
         sprintf(name,"%s",(yyvsp[-3].id));
         if(inFunc==1){
           sprintf(name,"f%s",(yyvsp[-3].id));
         }
          gen_code( LD_VAR, context_check(name)+(yyvsp[-1].intval) ); }
#line 1929 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 441 "senzillc.y" /* yacc.c:1646  */
    { gen_code( ADD, 0 ); }
#line 1935 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 442 "senzillc.y" /* yacc.c:1646  */
    { gen_code( SUB, 0 ); }
#line 1941 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 443 "senzillc.y" /* yacc.c:1646  */
    { gen_code( MULT, 0 ); }
#line 1947 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 444 "senzillc.y" /* yacc.c:1646  */
    { gen_code( DIV, 0 ); }
#line 1953 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 445 "senzillc.y" /* yacc.c:1646  */
    { gen_code( PWR, 0 ); }
#line 1959 "senzillc.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 446 "senzillc.y" /* yacc.c:1646  */
    {
            char name[strlen((yyvsp[-3].id))];
            sprintf(name,"%s",(yyvsp[-3].id));
            char name2[strlen((yyvsp[-1].id))];
            sprintf(name2,"%s",(yyvsp[-1].id));
            if(inFunc==1){
             sprintf(name,"f%s",(yyvsp[-3].id));
             sprintf(name2,"f%s",(yyvsp[-1].id));
            }
            if(context_type(name)!= eINT && context_type(name2)!= eINT && context_type(name) == context_type(name2)){
              lenStr = context_length(name)+context_length(name2);
              gen_code( DATA, lenStr );
              loadStr(name,0);
              loadStr(name2,context_length(name));
            }
       }
#line 1980 "senzillc.tab.c" /* yacc.c:1646  */
    break;


#line 1984 "senzillc.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 465 "senzillc.y" /* yacc.c:1906  */


extern struct instruction code[ MAX_MEMORY ];

/*=========================================================================
MAIN
=========================================================================*/
int main( int argc, char *argv[] )
{
  extern FILE *yyin;
  if ( argc < 3 ) {
    printf("usage <input-file> <output-file>\n");
    return -1;
  }
  yyin = fopen( argv[1], "r" );
  /*yydebug = 1;*/
  errors = 0;
  printf("Senzill Compiler\n");
  yyparse ();
  printf ( "Parse Completed\n" );
  if ( errors == 0 )
    {
      //print_code ();
      //fetch_execute_cycle();
      write_bytecode( argv[2] );
    }
  return 0;
}

/*=========================================================================
YYERROR
=========================================================================*/
int yyerror ( char *s ) /* Called by yyparse on error */
{
  errors++;
  printf ("%s\n", s);
  return 0;
}
/**************************** End Grammar File ***************************/
